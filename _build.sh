#!/bin/sh
bundle install
bundle exec jekyll build
pandoc --pdf-engine=xelatex --listings --toc --number-sections -s -f markdown -o codebook.pdf _site/markdown-tpl.txt
