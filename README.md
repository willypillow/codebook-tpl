# Codebook Template

A codebook template for ICPC contests utilizing Jekyll and Pandoc, used by the team NoName@NTU.

## Dependencies
- Jekyll
- Pandoc
- Xelatex
- Related fonts

## How To

### Adding an category

Suppose that we would like to add a category titled "Graph" with the id `graph`. We can add the following YAML snippet under `collections` in the file `_config.yml`.

``` yaml
  graph:
    title: "Graph"
```

Then, create the directory `_graph/`.

### Adding a code snippet

Under the directory that corresponds to the category you would like to place the snippet in, create a directory with an arbitrary name. Under the directory, create the file `index` with the following content:

``` md
---
title: "The Title of the Snippet"
libpath: "_path_to_the_snippet_source.cpp"
testpath: "_path_to_the_test_source.cpp"
---

Whatever textual content, *supports Markdown syntax*.
```

Note that `libpath`, if not specified, defaults to `_lib.cpp`, and `testpath` defaults to `_test.cpp`.

It then remains to place the source code under the specified path. Note that the test file will not be displayed in the final PDF, and exists only for automatic testing, a possible future feature.

### PDF Generation

Run `./build.sh` to generate the codebook PDF at `codebook.pdf`.

## TODO

- Automatic tests
